#!/bin/bash
# Usage - runner.sh URL USER PASSWORD STATEFILE

URL=$1
LFTP_USER=$2
LFTP_PASS=$3
LFILE_PREFIX=$4
DEBUG_WAIT=$5
DEBUG_WAIT_TIMES=$6
#echo "Url: $1, User: $2 Pass: $3. Prefix: $4"
touch $LFILE_PREFIX.lock
#OUTPUT=$(curl -k $URL --user $LFTP_USER:$LFTP_PASS 2>&1 )
curl -k $URL --silent --user $LFTP_USER:$LFTP_PASS 2>&1 > $LFILE_PREFIX.lock
#sleep 6000
SFTP_STATUS=$?

####DEBUGGING
if [ ! -z $DEBUG_WAIT ]
then
  if [ ! -z $DEBUG_WAIT_TIMES ]
  then
    x=1
    while [ $x -gt $DEBUG_WAIT_TIMES ];
    do
    x=$(($x+1))
    sleep $DEBUG_WAIT
    done
  fi
fi
#echo "curl -k $URL --user '$LFTP_USER:$LFTP_PASS' > $LFILE_PREFIX.cache"

if [ $SFTP_STATUS != '0' ]
  then
    touch $LFILE_PREFIX.err
    echo $SFTP_STATUS > $LFILE_PREFIX.err
    mv -f $LFILE_PREFIX.lock $LFILE_PREFIX.lock.err
    exit $SFTP_STATUS
  else
    #OUTPUT_FILE="$(echo $LFILE_PREFIX).STATEFILE"
    #echo "$OUTPUT" > $OUTPUT_FILE
    mv -f $LFILE_PREFIX.lock $LFILE_PREFIX.STATEFILE
fi
