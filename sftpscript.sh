#!/bin/bash
#
#Please see check_sftp.cfg
#
#
CONFIGFILE="$1"
CACHEFILE="check_sftp.cache"
STATUS=0
STATUS_TEXT=OK
ERROR=0#
NR_OF_WARNINGS=0
NR_OF_CRITICALS=0

#MYPATH="/usr/local/bin"
#Arrays where director names and amount of files are stored in
declare -a dirs_from_ftp
declare -a files_from_ftp
declare -a dir_warn_index
declare -a dir_warn_name
declare -a dir_warn_value
declare -a dir_err_value
declare -a file_mod_date
declare warningdirs_from_ftp
declare metrics
declare long_notification_text
declare -a skipped_dirs

search_index(){
#Search through dir_warn_name array and return the value from the same index in dir_warn_value
  search_counter=0
  if [ "$1" = "w" ]
  then
    for search_name in "${dir_warn_name[@]}"; do
      if [ "$search_name" = "$2" ]; then
        echo ${dir_warn_value[$search_counter]}
        return
      else
        ((search_counter++))
      fi
    done
  elif [ $1 = "e" ]
  then
    for search_name in "${dir_warn_name[@]}"; do
      if [ "$search_name" = "$2" ]; then
        echo ${dir_err_value[$search_counter]}
        return
      else
        ((search_counter++))
      fi
    done
  fi
}

add_warning_message() {
  ##Add the directorynames and count of files to a variable so we can report in check_mk
  ERROR_OUTPUT="$ERROR_OUTPUT $1"
  ((NR_OF_WARNINGS++))
}

add_critical_message() {
  ERROR_OUTPUT="$ERROR_OUTPUT $1"
  ((NR_OF_CRITICALS++))
}

add_notification_message() {
  OUTPUT="$OUTPUT $1"
}

add_long_message() {
  long_notification_text="$long_notification_text $1\\\n"
}

fetch_sftp() {
  if [ -v $dir ]
   then
     FILE_PREFIX=$CACHEPATH/sftp.root
   else
     FILE_PREFIX=$CACHEPATH/$dir
  fi
  if [ ! -z $dir ]
  then
    nohup bash $RUNNERFLAGS $MYPATH/runner.sh sftp://$FTP_HOSTNAME/~/$dir/ $FTP_USER $FTP_PASS $FILE_PREFIX &
    FTPRETVAL=$?
    sleep 1
  else
    bash $RUNNERFLAGS $MYPATH/runner.sh sftp://$FTP_HOSTNAME/~/ $FTP_USER $FTP_PASS $FILE_PREFIX
    FTPRETVAL=$?
    sleep 1
fi
}
parse_warnings(){
#Parse the $WARNING_THRESHOLD variable make 3 arrays, key, value, index
#Using associative arrays would be better and a lot nicer, for some reason bash coulnt use it normally
#Therefore i consider it unreliable for monitoring a production environment
  local kv_counter=0
  for kv_pair in $WARNING_THRESHOLD
  do
     while IFS=, read -r key warning_th error_th
     do
       #echo "Key: $key  Value: $value PAIR: $kvpair"
       dir_warn_name[$kv_counter]=$key
       dir_warn_value[$kv_counter]=$warning_th
       dir_err_value[$kv_counter]=$error_th
       dir_warn_index[$kv_counter]=$kv_counter

     done <<< "$kv_pair"
     kv_counter=$((kv_counter+1))
  done
}


get_and_compare_dir_age(){
  if [ -z $CHECK_DIR_AGE ]
  then
    if [ "$CHECK_DIR_AGE" == "on" ]
    then
      #echo "Got 1: $1"
      #echo "Got 2: $2"
      dir_mod_epoch=$(date -d "$2" +%s)
      current_epoch=$(date +%s)
      time_diff=$(($current_epoch - $dir_mod_epoch))
      #echo "$dir is $time_diff seconds old"
      if [ "$time_diff" -gt "$MAX_FILE_AGE" ]
      then
          add_warning_message "Directory $dir has not been modified for more than $MAX_FILE_AGE seconds.  "
          ((NR_OF_WARNINGS++))
      fi
    fi
  fi
}

fetch_sftp_dir() {
  if [ -f "$dir.cache" ]
  then
    add_notification_message "$dir ls running"
  else
    fetch_sftp
  fi
}

count_files_and_get_dates(){

  if [ -f $CACHEPATH/$dir.lock ]
  then
    lock_epoch=$(date +%s -r $CACHEPATH/$dir.lock)
    current_Seconds=$(date +%s)
    lock_age=$((current_Seconds - lock_epoch))
    if [ $lock_age -gt 2400 ]
    then
      log_remove=$(rm $CACHEPATH/$dir.lock)
      RETVAL=$?
      if [ $RETVAL -eq 0 ]
      then
        add_notification_message "$dir curl crashed? removing lock"
      else
        full_cache_path=$(readlink -f $CACHEPATH/$dir.lock)
        add_warning_message "$dir cannot remove way to old cache: check $full_cache_path permissions on $HOSTNAME "
        add_long_message "Check is not reprting reliable data: i can do nothing with $full_cache_path  Please check permissions"

      fi
    else
      add_notification_message "$dir curl run."
    fi
  else
      last_Modification_Seconds=$(date +%s -r $CACHEPATH/$dir.STATEFILE)
      current_Seconds=$(date +%s)
      file_age=$((current_Seconds - last_Modification_Seconds))
      if [ $file_age -gt $REFRESHTIME ]
      then
        fetch_sftp
      else
        add_long_message "$dir stats from cache. $file_age seconds old"
      fi
  fi

  if [ -f $CACHEPATH/$dir.STATEFILE ]
  then
    #Get a listing of directory stored in $dir and parse it
    unset file_mod_date #First clear things up
    IFS_BAK=$IFS
    IFS=$'\n'
    for mod in $(cat $CACHEPATH/$dir.STATEFILE | grep "^-" | awk '{print $6 " " $7 " " $8}')
    #Produces last files mtime exp: May 7 09:53
    do
      if [ "$DEEP_DIVE" != "0" ]
      then
        epochmod=$(date -d "${mod}" +%s)
        file_mod_date+=($epochmod)
      fi
      countfiles_from_ftp=$((countfiles_from_ftp+1))
    done
    IFS=$IFS_BAK
  else
    add_notification_message "$dir no cache yet"
  fi

}

scan_old_files(){
  if [ -v file_mod_date[@] ]
  then
    OLD_IFS=$IFS
    current_epoch=$(date +%s)
    IFS=$'\n'
    last_mod_date=$(echo "${file_mod_date[*]}" | sort -rn | head -n1)
    time_diff=$(($current_epoch - $last_mod_date))
    IFS=$OLD_IFS
  fi
}
check_if_skip_needed(){
  if [ -z $DIR_NEEDS_SKIP ]
  then
    DIR_NEEDS_SKIP=0
  else
    DIR_NEEDS_SKIP=0
  fi
  #See if we need to skip this dir
  for SKIP in $SKIP_DIR
  do
  #echo "($1) in ($SKIP)"
    if [ $SKIP == $1 ]
    then
      add_long_message "$dir skipped."
      DIR_NEEDS_SKIP=1
      skipped_dirs[$loop_counter]=$dir
    fi
  done
}
scan_for_metrics_and_errors(){
  for ((m = 1; m <= $loop_counter; m++));
  do
     #Fetch values from WARNING_THRESHOLD and see if our dir is in there
     report_warning_threshold=$(search_index w ${dirs_from_ftp[$m]})
     report_error_threshold=$(search_index e ${dirs_from_ftp[$m]})
     #Fill the metrics string  IF the mentioned directory is not on the skip list
     if [ "${dirs_from_ftp[$m]}" != "${skipped_dirs[$m]}" ]
     then
       metrics="${metrics}${dirs_from_ftp[$m]}=${files_from_ftp[$m]};$report_warning_threshold;$report_error_threshold|"
       #See if were om the end of the array so we dont have do output a last pipe sign
       #if [ $m != $loop_counter ];
       #  then
       #   metrics="${metrics}|"
       # fi
     fi


     #See if we need to alert someone. We'll collect all the dirs that go over the threshold in one variable $ERROR_OUTPUT.
     #So we can report it later
     if [ -z $report_error_threshold ]
     then
        if [ ! -z $report_warning_threshold ];
        then
            if [ "${files_from_ftp[$m]}" -gt "$report_warning_threshold" ];
            then
                add_warning_message "${dirs_from_ftp[$m]}:${files_from_ftp[$m]}/$report_warning_threshold/-"
            fi
        fi
      else
        if [ "${files_from_ftp[$m]}" -gt "$report_error_threshold" ];
        then
            add_critical_message "${dirs_from_ftp[$m]}:${files_from_ftp[$m]}/$report_warning_threshold/$report_error_threshold"
        elif [ "${files_from_ftp[$m]}" -gt "$report_warning_threshold" ]
        then
            add_warning_message "${dirs_from_ftp[$m]}:${files_from_ftp[$m]}/$report_warning_threshold/$report_error_threshold"
        fi
     fi



     #IF $DEEPDIVE is enabled, scan all the files individualy (by calling scan_old_files) and compare time diff)
     if [ "$DEEP_DIVE" != "0" ]
     then
        scan_old_files
        if [ ! ${#file_mod_date[@]} -eq 0 ]
        then
          if [ "$time_diff" -gt "$MAX_FILE_AGE" ];
          then
            add_warning_message "${dirs_from_ftp[$m]} has not been modified in $MAX_FILE_AGE seconds. "
            ((NR_OF_WARNINGS++))
          else
            time_diff=0
          fi
        fi
     fi


done
}
scan_sftp(){
  OLD_IFS=$IFS
  loop_counter=0
  IFS=$','
  while read dir dir_mod_date
  do
    #loop_counter is used as an index for the arrays
    loop_counter=$((loop_counter+1))
    #if a directory is mentioned in WARNING_THRESHOLD, skip it
    check_if_skip_needed $dir
    if [ $DIR_NEEDS_SKIP != 1 ]
    then

      #if a directory is NOT mentioned in WARNING_THRESHOLD, process it
      countfiles_from_ftp=0 #Just reset is, to be sure

      #Fill array dirs_from_ftp with all directory names
      dirs_from_ftp[loop_counter]=$dir

      if [ "$DEEP_DIVE" = "0" ]
      then
        #echo "($dir) date ($dir_mod_date)"
        get_and_compare_dir_age "$dir" "$dir_mod_date"
      fi

      count_files_and_get_dates

      #Fill array files_from_ftp with the amount of files
      files_from_ftp[loop_counter]=$countfiles_from_ftp
    else
      #Just fill the variables
      dirs_from_ftp[loop_counter]="$dir"
      files_from_ftp[loop_counter]=0
    fi
  done < <(cat $CACHEPATH/sftp.root.STATEFILE | grep '^d' | egrep '^[^\.]*$' | awk '{print $9 "," $6 " " $7 " " $8}')
  IFS=$OLD_IFS
}

###########################################################
#                                                         #
#               M A I N  C O D E                          #
#                                                         #
###########################################################
if [ -z "$CONFIGFILE" ]
then
  echo "Configfile not found"
  exit 2
fi
source $CONFIGFILE > /dev/null 2>&1
if [ $? != '0' ]
  then
  echo "Error parsing config"
  exit 2
fi
fetch_sftp
if [ "$FTPRETVAL" = '0' ]
then
  parse_warnings
  scan_sftp
  scan_for_metrics_and_errors
  metrics=${metrics%?};
  add_long_message "cfg used: $CONFIGFILE"
  if [ ! -z $CHECK_DIR_AGE ]
  then
    add_long_message "Dir age check turned off"
  elif [[ ! "$CHECK_DIR_AGE" == "1" ]];
  then
    add_long_message "Dir age check turned off"
  fi
    #statements

else
  STATUS=2
  STATUS_TEXT="CRIT"
  OUTPUT="Cannot connect to SFTP server"
fi
#If errors where reported, report them in the output of this script
if [ "$NR_OF_CRITICALS" -eq 0 ]
then
  if [ "$NR_OF_WARNINGS" -gt 0 ];
  then
    STATUS=1
    STATUS_TEXT="WARNING"
    OUTPUT="$OUTPUT $ERROR_OUTPUT"
  else
    true
  fi
else
  STATUS=2
  STATUS_TEXT="CRITICAL"
  OUTPUT="$OUTPUT $ERROR_OUTPUT"
fi

echo "<<<local>>>"
echo "$STATUS $CHECK_NAME $metrics $STATUS_TEXT - $OUTPUT\\n$long_notification_text"
