#!/bin/bash
#-------BEGIN SCRIPT CONFIG--------------
CHECK_NAME="SFTP_IVSNEXT"      # Name to show in CheckMK
FTP_HOSTNAME=192.168.122.214  # Hostname/ip to monitor
FTP_USER=sftp                 # Username to login
FTP_PASS=dXbWkncz             # Password to login
DEEP_DIVE=0 #Determines if age need to be determined on dir's mtime or mtime of files that are in dirs
MAX_FILE_AGE=600 #Maximum age of a file/dir before we start complaining
##Here thresholds for certain amount of file can be configured.
#Syntax <directory-name>,<max-files>
#Example:
#In,200
#Out,100
WARNING_THRESHOLD="
In,200
Out,20
Cas,200
"
SKIP_DIR="
Archive
"
#------END SCRIPT CONFIG------------------


STATUS=0
STATUS_TEXT=OK
ERROR=0
NR_OF_ERRORS=0

#Arrays where director names and amount of files are stored in
declare -a dirs_from_ftp
declare -a files_from_ftp
declare -a dir_warn_index
declare -a dir_warn_name
declare -a dir_warn_value
declare -a file_mod_date
declare warningdirs_from_ftp
declare metrics


do_sftp_curl(){
  #Launch curl and list the given dirs
  curl --silent -k "sftp://$FTP_HOSTNAME/$1" --user "$FTP_USER:$FTP_PASS $2 $3 $4"
  SFTP_STATUS=$?
}

parse_warnings(){
#Parse the $WARNING_THRESHOLD variable make 3 arrays, key, value, index
#Using associative arrays would be better and a lot nicer, for some reason bash coulnt use it normally
#Therefore i consider it unreliable for monitoring a production environment
  local kv_counter=0
  for kv_pair in $WARNING_THRESHOLD
  do
     while IFS=, read -r key value
     do
       #echo "Key: $key  Value: $value PAIR: $kvpair"
       dir_warn_name[$kv_counter]=$key
       dir_warn_value[$kv_counter]=$value
       dir_warn_index[$kv_counter]=$kv_counter

     done <<< "$kv_pair"
     kv_counter=$((kv_counter+1))
  done
}


search_index(){
#Search through dir_warn_name array and return the value from the same index in dir_warn_value
  search_counter=0
  for search_name in "${dir_warn_name[@]}"; do
    if [ "$search_name" = "$1" ]; then
      echo ${dir_warn_value[$search_counter]}
      return
    else
      ((search_counter++))
    fi
  done
}

check_if_skip_needed(){
  #See if we need to skip this dir
  for SKIP in $SKIP_DIR
  do
    if [ "$SKIP" == "$1" ]
    then
      DIR_NEEDS_SKIP=1
    else
      DIR_NEEDS_SKIP=0
    fi
  done

}

count_files_and_get_dates(){
  #Get a listing of directory stored in $dir and parse it
  unset file_mod_date #First clear things up
  IFS_BAK=$IFS
  IFS=$'\n'
  for mod in $(do_sftp_curl '~/'$dir/ | grep "^-" | awk '{print $6 " " $7 " " $8}')
  #Produces last files mtime exp: May 7 09:53
  do
   if [ "$DEEP_DIVE" != "0" ]
   then
     epochmod=$(date -d "${mod}" +%s)
     file_mod_date+=($epochmod)
   fi
   countfiles_from_ftp=$((countfiles_from_ftp+1))
  done
  IFS=$IFS_BAK
}

scan_sftp(){
  OLD_IFS=$IFS
  loop_counter=0
  IFS=$','
  while read dir dir_mod_date
  ## Loop for counting files in dirs
  do
    #loop_counter is used as an index for the arrays
    loop_counter=$((loop_counter+1))
    #if a directory is mentioned in WARNING_THRESHOLD, skip it
    check_if_skip_needed $dir
    if [ $DIR_NEEDS_SKIP != 1 ]
    then

      #if a directory is NOT mentioned in WARNING_THRESHOLD, process it
      countfiles_from_ftp=0 #Just reset is, to be sure

      #Fill array dirs_from_ftp with all directory names
      dirs_from_ftp[loop_counter]=$dir

      if [ "$DEEP_DIVE" = "0" ]
      then
        get_and_compare_dir_age '$dir $dir_mod_date'
      fi

      count_files_and_get_dates

      #Fill array files_from_ftp with the amount of files
      files_from_ftp[loop_counter]=$countfiles_from_ftp
    else
      #Just fill the variables
      dirs_from_ftp[loop_counter]="$dir"
      files_from_ftp[loop_counter]=0
    fi
  done < <(do_sftp_curl '~' | grep '^d' | egrep '^[^\.]*$' | awk '{print $9 "," $6 " " $7 " " $8}')
  IFS=$OLD_IFS
}

get_and_compare_dir_age(){
  dir_mod_epoch=$(date -d "$2" +%s)
  current_epoch=$(date +%s)
  time_diff=$(expr $current_epoch - $dir_mod_epoch)
  if [ "$time_diff" -gt "$MAX_FILE_AGE" ]
  then
    add_error_dir "$dir mtime more than $MAX_FILE_AGE seconds.  "
    ((NR_OF_ERRORS++))
  fi
}

scan_old_files(){
  #If $DEEPDIVE is not 0. Scan the files mtime individually
  OLD_IFS=$IFS
  current_epoch=$(date +%s)
  IFS=$'\n'
  last_mod_date=$(echo "${file_mod_date[*]}" | sort -rn | head -n1)
  time_diff=$(expr $current_epoch - $last_mod_date)
  IFS=$OLD_IFS
}

scan_for_metrics_and_errors(){
  for ((m = 1; m <= $loop_counter; m++));
  do
     #Fetch values from WARNING_THRESHOLD and see if our dir is in there
     report_threshold=$(search_index ${dirs_from_ftp[$m]})
     #Fill the metrics string
     metrics="${metrics}${dirs_from_ftp[$m]}=${files_from_ftp[$m]};$report_threshold;$CRIT"
     #See if were om the end of the array so we dont have do output a last pipe sign
     if [ $m != $loop_counter ];
      then
         metrics="${metrics}|"
     fi
     #See if we need to alert someone. We'll collect all the dirs that go over the threshold in one variable $ERROR_OUTPUT.
     #So we can report it later
     if [ ! -z $report_threshold ];
     then
       if [ "${files_from_ftp[$m]}" -gt "$report_threshold" ];
       then
         add_error_dir "${dirs_from_ftp[$m]}:${files_from_ftp[$m]}/$report_threshold"
         ((NR_OF_ERRORS++))
       fi
     fi
     #IF $DEEPDIVE is enabled, scan all the files individualy (by calling scan_old_files) and compare time diff)
     if [ "$DEEP_DIVE" != "0" ]
     then
        scan_old_files
        if [ "$time_diff" -gt "$MAX_FILE_AGE" ];
        then
          add_error_dir "${dirs_from_ftp[$m]} no updates in $MAX_FILE_AGE seconds. "
          ((NR_OF_ERRORS++))

        else
          time_diff=0
        fi
     fi

     #If errors where reported, report them in the output of this script
     if [ "$NR_OF_ERRORS" -gt 0 ];
     then
       STATUS=1
       STATUS_TEXT="WARNING"
       OUTPUT=$ERROR_OUTPUT
     else
       OUTPUT="All OK"
   fi
done
}

add_error_dir() {
  ##Add the directorynames and count of files to a variable so we can report in check_mk
  ERROR_OUTPUT="$ERROR_OUTPUT $1"
}



###################################################################################################################
#                                                                                                                 #
#                                                BEGIN MAIN CODE                                                  #
#                                                                                                                 #
###################################################################################################################

#Check if we are able to setup an connection, if not, exit and error out
do_sftp_curl '~' >> /dev/null
if [ $SFTP_STATUS -eq "0" ];
then
  #if we can, proceed
  parse_warnings
  scan_sftp
  scan_for_metrics_and_errors
else
  #If we cannot error out and report
  STATUS=2
  STATUS_TEXT="CRIT"
  OUTPUT="Error talking to sftp server"
fi
#Output something that CheckMK can understand
echo "$STATUS $CHECK_NAME $metrics $STATUS_TEXT - $OUTPUT"
